*** Variable ***
### Opções necessárias para rodar headless no Linux do CI (runner)
${OPTIONS}     add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")
#${OPTIONS}     add_experimental_option('excludeSwitches',['enable-logging'])

###   ELEMENTOS   ###
&{URL}      SAUCE_DEMO=https://www.saucedemo.com/   
   ...      AUTOMATION=http://demo.automationtesting.in/Register.html
   ...      SAUCE_DEMO_INVENTORY=https://www.saucedemo.com/inventory.html
   ...      AUTOMATION_PRACTICE=http://automationpractice.com/index.php

&{TEXT}     USER_NAME=id:user-name
   ...      PASSWORD=id:password
   ...      DEMO_FIRST=xpath://input[@placeholder="First Name"]
   ...      DEMO_LAST=xpath://input[@placeholder="Last Name"]
   ...      DEMO_ADRESS=xpath://textarea[@ng-model="Adress"]
   ...      DEMO_EMAIL=xpath://input[@ng-model="EmailAdress"]   
   ...      DEMO_PHONE=xpath://input[@ng-model="Phone"]
   ...      DEMO_PASSWORD=xpath://input[@id="firstpassword"]
   ...      DEMO_PASSWORD_CONFIRM=xpath://input[@id="secondpassword"]
   ...      AUTOMATION_PRACTICE_SOURCE=xpath://input[@placeholder="Search"]

&{BUTTON}   LOGIN=id:login-button
      ...   SAUCE_ADD_TO_CARD=xpath://button[text()="ADD TO CART"]
      ...   PRACTICE_SOURCE=xpath://button[@name="submit_search"]
      ...   PRACTICE_ADD_TO_CARD=xpath://span[text()="Add to cart"]
      ...   PRACTICE_PROCEED_CHECKOUT=xpath://a[@title="Proceed to checkout"]
      ...   SUBMIT_DEMO=xpath://button[@id="submitbtn"]

&{SPAN}     RETORNO_ACAO=xpath://h3[@data-test="error"]

&{DIV}      LANGUAGE=xpath://div[@id="msdd"]
   ...      SELECT_COUNTRY=xpath://span[@class="select2-selection select2-selection--single"]

&{LINK}     PRODUTO_EXEMPLO=id:item_5_title_link
    ...     CARRINHO_COMPRAS=xpath://a[@class="shopping_cart_link fa-layers fa-fw"]
    ...     LANGUAGE_ENGLISH=xpath://a[text()="English"]
    ...     PRACTICE_FADED_SHORT_SLEEVE_TSHIRTS=xpath://a[text()="Faded Short Sleeve T-shirts"]
    ...     PRACTICE_PRINTED_CHIFFON_DRESS=xpath://a[text()="Printed Chiffon Dress"]

&{RADIO}    GENDER=radiooptions

&{CHECKBOX}    HOBBIES_CRICKET=xpath://input[@id="checkbox1"]

&{SELECT}      SKILL=xpath://select[@id="Skills"]
      ...      COUNTRY=xpath://select[@id="countries"]
      ...      SELECT_COUNTRY=xpath://input[@class="select2-search__field"]
      ...      ANO_BIRTH=xpath://select[@placeholder="Year"]
      ...      MES_BIRTH=xpath://select[@placeholder="Month"]
      ...      DIA_BIRTH=xpath://select[@placeholder="Day"]

&{IMG}   FADEDSHORTSLEEVETSHIRTS=//img[@title="Faded Short Sleeve T-shirts"]

###   EXEMPLOS DOS TESTES   ###
&{exemplo_sauce}    user_correto=standard_user         
             ...    user_locked=locked_out_user
             ...    password=secret_sauce

&{mensagem}         usuario_bloqueado=Epic sadface: Sorry, this user has been Locked out - erro.
        ...         practice_product_add_sucessfully=Product successfully added to your shopping cart


&{cadastro_Demo}  nome=Roberto   
             ...  last_name=Farias  
             ...  adress=Rua Monge,17
             ...  email=roberto.farias@gmail.com
             ...  Phone=9876546317
             ...  Gender=FeMale
             ...  Skill=Content Management Systems (CMS)
             ...  Country=Cambodia
             ...  Select_country=Japan
             ...  ano_birth=1982
             ...  mes_birth=March
             ...  dia_birth=18
             ...  password=Teste123

&{exemplo_practice}     FadedShortSleeveTshirts=Faded Short Sleeve T-shirts
               ...      PrintedChiffonDress=Printed Chiffon Dress